#include <stdio.h>
#include <strings.h>
#include <winsock2.h>
#include <stdint.h>
#include <stdlib.h>
#include <dirent.h>

void *doReceiving(void *sockID);
BOOL InitWinSock2_0();

char szServerIPAddr[20] = "192.168.12.167"; // Dia chi ID cua server
int nServerPort = 21;


int main()
{
    char *user = "hungdaica";
    char *pass = "hung";

    // char *user = malloc(110);
    // char *pass = malloc(110);
    // char szServerIPAddr[20];
    char fileName[2048];

    /*printf("Enter IP address: ");
    scanf("%s", szServerIPAddr);
    printf("Enter Username: ");
    scanf("%s", user);
    printf("Enter password: ");
    scanf("%s", pass);*/

    printf("Enter file name: ");
    scanf("%s", fileName);


    //SOCKET
    printf("\n%s\t%d\n", szServerIPAddr, nServerPort);

    if (!InitWinSock2_0())
    {
        printf("Unable to Initialize Windows Socket environment, ERRORCODE = %d\n",
               WSAGetLastError());
        return -11;
    }

    SOCKET hClientSocket;
    hClientSocket = socket(AF_INET, SOCK_STREAM, 0);
    printf("%d\n", hClientSocket);
    if (hClientSocket == INVALID_SOCKET)
    {
        printf("Unable to create Server socket\n");
        WSACleanup();
        return -12;
    }

    struct sockaddr_in serverAddr;

    serverAddr.sin_family = AF_INET;
    serverAddr.sin_addr.s_addr = inet_addr(szServerIPAddr);
    serverAddr.sin_port = htons(nServerPort);

    if (connect(hClientSocket, (struct sockaddr *)&serverAddr,
                sizeof(serverAddr)) < 0)
    {
        printf("Unable to connect to %s on port %d\n", szServerIPAddr, nServerPort);
        closesocket(hClientSocket);
        WSACleanup();
        return -13;
    }

    printf("Connection established ............\n");


    //START
    char *szBuffer = malloc(2048);
    recv(hClientSocket, szBuffer, 2048, 0);
    printf("Received: %s\n", szBuffer);
    if (strstr(szBuffer, "220") == NULL)
    {
        return 220;
    }

    // char data[2048];

    // USERNAME
    printf("220 Hello from server\n");
    char send1[2048];
    sprintf(send1, "USER %s\r\n", user);
    send(hClientSocket, send1, strlen(send1), 0);
    printf("Send username: %s\n", user);
    char data1[2048];
    recv(hClientSocket, data1, 2048, 0);
    printf("Received: %s\n", data1);

    // PASSWORD
    char send2[2048];
    sprintf(send2, "PASS %s\r\n", pass);
    send(hClientSocket, send2, strlen(send2), 0);
    printf("Send password: %s\n", pass);
    char data2[2048];
    recv(hClientSocket, data2, 2048, 0);
    printf("Received: %s\n", data2);

    // FEATURE
    send(hClientSocket, "FEAT\r\n", strlen("FEAT\r\n"), 0);
    printf("Send FEAT\n");
    char data3[2048];
    recv(hClientSocket, data3, 2048, 0);
    printf("Received: %s\n", data3);

    // CWD
    send(hClientSocket, "CWD /\r\n", strlen("CWD /\r\n"), 0);
    printf("Send CWD /BT2/file\n");
    char data7[2048];
    recv(hClientSocket, data7, 2048, 0);
    printf("Received: %s\n", data7);

    // PWD
    send(hClientSocket, "PWD\r\n", strlen("PWD\r\n"), 0);
    printf("Send PWD\n");
    char data4[2048];
    recv(hClientSocket, data4, 2048, 0);
    printf("Received: %s\n", data4);


    // SET TYPE
    send(hClientSocket, "TYPE A\r\n", strlen("TYPE A\r\n"), 0);
    printf("Send TYPE\n");
    char data5[2048];
    recv(hClientSocket, data5, 2048, 0);
    printf("Received: %s\n", data5);

    // PASSIVE MODE
    send(hClientSocket, "PASV\r\n", strlen("PASV\r\n"), 0);
    printf("Send PASV\n");
    char data6[2048];
    recv(hClientSocket, data6, 2048, 0);
    printf("Received: %s\n", data6);

    // char *data = "gigido";
    // printf("%d\t%s\n", i, szServerIPAddr);

    // CACULATE PORT NUMBER
    int port = 0, port1 = 0, port2 = 0, dem = 0;
    for (int i = 0; i < strlen(data6); i++)
    {
        if (data6[i] == ',')
        {
            dem++;
            continue;
        }
        if (dem == 4)
        {
            port1 = port1 * 10 + data6[i] - 48;
        }
        if (data6[i] == (')'))
            break;
        if (dem == 5)
        {
            port2 = port2 * 10 + data6[i] - 48;
        }
    }
    port = port1 * 256 + port2;
    printf("Received port: %d\n", port);


    // NEW SOCKET
    SOCKET hClientSocket2;
    hClientSocket2 = socket(AF_INET, SOCK_STREAM, 0);
    printf("%d\n", hClientSocket2);
    if (hClientSocket2 == INVALID_SOCKET)
    {
        printf("Unable to create Server socket\n");
        WSACleanup();
        return -12;
    }

    struct sockaddr_in serverAddr2;

    serverAddr2.sin_family = AF_INET;
    serverAddr2.sin_addr.s_addr = inet_addr(szServerIPAddr);
    serverAddr2.sin_port = htons(port);

    if (connect(hClientSocket2, (struct sockaddr *)&serverAddr2,
                sizeof(serverAddr2)) < 0)
    {
        printf("Unable to connect to %s on port %d\n", szServerIPAddr, port);
        closesocket(hClientSocket2);
        WSACleanup();
        return -13;
    }

    printf("Connection 2 established ............\n");

    /*// send_file(hClientSocket2);
    FILE *File = NULL;
    File = fopen(fileName, "rb");
    if (!File)
    {
        printf("Error while readaing the file\n");
    }
    else
    {
        // printf ("File opened successfully!\n");
        fseek(File, 0, SEEK_END);
        unsigned long Size = ftell(File);
        fseek(File, 0, SEEK_SET);
        char *Buffer = malloc(Size);

        fread(Buffer, Size, 1, File);
        fclose(File);
        send(hClientSocket2, Buffer, Size, 0); // File Binary
        printf("Sent: *data_file*\n\n");
        free(Buffer);
        // printf ("File sent successfully!\n");
    }*/

    send(hClientSocket,"MLSD\r\n", strlen("MLSD\r\n"), 0);
    printf("Send MLSD\n");
    Sleep(1000);
    char* data10 = malloc(2048);
    recv(hClientSocket2, data10, 2048, 0);
    printf("Received: %s\n", data10);

    closesocket(hClientSocket2);

    printf("\n");

    
    char data9[2048];
    recv(hClientSocket, data9, 2048, 0);
    printf("Received: %s\n", data9);

    
    




    // // CWD
    // send(hClientSocket, "CWD /\r\n", strlen("CWD /\r\n"), 0);
    // printf("Send CWD /BT2/crash\n");
    // char data8[2048];
    // recv(hClientSocket, data8, 2048, 0);
    // printf("Received: %s\n", data8);

    // // PWD
    // send(hClientSocket, "PWD\r\n", strlen("PWD\r\n"), 0);
    // printf("Send PWD\n");
    // char data15[2048];
    // recv(hClientSocket, data15, 2048, 0);
    // printf("Received: %s\n", data15);

    // // SET TYPE
    // send(hClientSocket, "TYPE A\r\n", strlen("TYPE A\r\n"), 0);
    // printf("Send TYPE\n");
    // char data12[2048];
    // recv(hClientSocket, data12, 2048, 0);
    // printf("Received: %s\n", data12);

    // PASSIVE MODE
    send(hClientSocket, "PASV\r\n", strlen("PASV\r\n"), 0);
    printf("Send PASV\n");
    char data13[2048];
    recv(hClientSocket, data13, 2048, 0);
    printf("Received: %s\n", data13);

    // char *data = "gigido";
    // printf("%d\t%s\n", i, szServerIPAddr);

    // CACULATE PORT NUMBER
    int port_ = 0, port1_ = 0, port2_ = 0, dem_ = 0;
    for (int i = 0; i < strlen(data13); i++)
    {
        if (data13[i] == ',')
        {
            dem_++;
            continue;
        }
        if (dem_ == 4)
        {
            port1_ = port1_ * 10 + data13[i] - 48;
        }
        if (data13[i] == (')'))
            break;
        if (dem_ == 5)
        {
            port2_ = port2_ * 10 + data13[i] - 48;
        }
    }
    port_ = port1_ * 256 + port2_;
    printf("Received port: %d\n", port_);

    // STOR
    char send3[2048];
    sprintf(send3, "STOR %s\r\n", fileName);
    send(hClientSocket, send3, strlen(send3), 0);
    printf("Send %s\n", send3);
    char data14[2048];
    recv(hClientSocket, data14, 2048, 0);
    printf("Received: %s\n", data14);

    // NEW SOCKET
    SOCKET hClientSocket3;
    hClientSocket3 = socket(AF_INET, SOCK_STREAM, 0);
    printf("%d\n", hClientSocket3);
    if (hClientSocket3 == INVALID_SOCKET)
    {
        printf("Unable to create Server socket\n");
        WSACleanup();
        return -12;
    }

    struct sockaddr_in serverAddr3;

    serverAddr3.sin_family = AF_INET;
    serverAddr3.sin_addr.s_addr = inet_addr(szServerIPAddr);
    serverAddr3.sin_port = htons(port_);

    if (connect(hClientSocket3, (struct sockaddr *)&serverAddr3,
                sizeof(serverAddr3)) < 0)
    {
        printf("Unable to connect to %s on port %d\n", szServerIPAddr, port_);
        closesocket(hClientSocket3);
        WSACleanup();
        return -13;
    }

    printf("Connection 3 established ............\n");

    // send_file(hClientSocket3);
    FILE *File = NULL;
    File = fopen(fileName, "rb");
    if (!File)
    {
        printf("Error while readaing the file\n");
    }
    else
    {
        // printf ("File opened successfully!\n");
        fseek(File, 0, SEEK_END);
        unsigned long Size = ftell(File);
        fseek(File, 0, SEEK_SET);
        char *Buffer = malloc(Size);

        fread(Buffer, Size, 1, File);
        fclose(File);
        send(hClientSocket3, Buffer, Size, 0); // File Binary
        printf("Sent: *data_file*\n\n");
        free(Buffer);
        // printf ("File sent successfully!\n");
    }

    closesocket(hClientSocket3);

    printf("\n");

    
    char data16[2048];
    recv(hClientSocket, data16, 2048, 0);
    printf("Received: %s\n", data16);

    
    
    /*// CWD
    send(hClientSocket, "CWD /BT2/file\r\n", strlen("CWD /BT2/file\r\n"), 0);
    printf("Send CWD /BT2/file\n");
    char data17[2048];
    recv(hClientSocket, data17, 2048, 0);
    printf("Received: %s\n", data17);

    // PWD
    send(hClientSocket, "PWD\r\n", strlen("PWD\r\n"), 0);
    printf("Send PWD\n");
    char data18[2048];
    recv(hClientSocket, data18, 2048, 0);
    printf("Received: %s\n", data18);

    // SET TYPE
    send(hClientSocket, "TYPE A\r\n", strlen("TYPE A\r\n"), 0);
    printf("Send TYPE\n");
    char data19[2048];
    recv(hClientSocket, data19, 2048, 0);
    printf("Received: %s\n", data19);*/

    // PASSIVE MODE
    send(hClientSocket, "PASV\r\n", strlen("PASV\r\n"), 0);
    printf("Send PASV\n");
    char data20[2048];
    recv(hClientSocket, data20, 2048, 0);
    printf("Received: %s\n", data20);

    // char *data = "gigido";
    // printf("%d\t%s\n", i, szServerIPAddr);

    // CACULATE PORT NUMBER
    int port_1 = 0, port1_1 = 0, port2_1 = 0, dem_1 = 0;
    for (int i = 0; i < strlen(data20); i++)
    {
        if (data20[i] == ',')
        {
            dem_1++;
            continue;
        }
        if (dem_1 == 4)
        {
            port1_1 = port1_1 * 10 + data20[i] - 48;
        }
        if (data20[i] == (')'))
            break;
        if (dem_1 == 5)
        {
            port2_1 = port2_1 * 10 + data20[i] - 48;
        }
    }
    port_1 = port1_1 * 256 + port2_1;
    printf("Received port: %d\n", port_1);




    //RETR
    char retr[2048];
    printf("Enter file you want to retreive: ");
    scanf("%s", retr);
    char send4[2048];
    sprintf(send4, "RETR %s\r\n", retr);
    send(hClientSocket, send4, strlen(send4), 0);
    printf("Send %s\n", send4);
    char data_[2048];
    recv(hClientSocket, data_, 2048, 0);
    printf("Received: %s\n", data_);



    // NEW SOCKET
    SOCKET hClientSocket4;
    hClientSocket4 = socket(AF_INET, SOCK_STREAM, 0);
    printf("%d\n", hClientSocket4);
    if (hClientSocket4 == INVALID_SOCKET)
    {
        printf("Unable to create Server socket\n");
        WSACleanup();
        return -12;
    }

    struct sockaddr_in serverAddr4;

    serverAddr4.sin_family = AF_INET;
    serverAddr4.sin_addr.s_addr = inet_addr(szServerIPAddr);
    serverAddr4.sin_port = htons(port_1);

    if (connect(hClientSocket4, (struct sockaddr *)&serverAddr4,
                sizeof(serverAddr4)) < 0)
    {
        printf("Unable to connect to %s on port %d\n", szServerIPAddr, port_1);
        closesocket(hClientSocket4);
        WSACleanup();
        return -13;
    }

    printf("Connection 4 established ............\n");

    // send_file(hClientSocket4);
    FILE *File1 = NULL;
    File1 = fopen(retr, "wb");
    if (!File1)
    {
        printf("Error while readaing the file\n");
    }
    else
    {
        // printf ("File opened successfully!\n");
        char *Buffer1 = malloc(10000);
        
        while (recv(hClientSocket4, Buffer1, strlen(Buffer1), 0) > 0)
        {
           fwrite(Buffer1, strlen(Buffer1), 1, File1);
        }
        
        //fread(Buffer, Size, 1, File1);
        fclose(File1);
        //send(hClientSocket4, Buffer, Size, 0); // File Binary
        printf("Sent: *data_file*\n\n");
        free(Buffer1);
        // printf ("File sent successfully!\n");
    }


    closesocket(hClientSocket4);

    printf("\n");

    
    char data21[2048];
    recv(hClientSocket, data21, 2048, 0);
    printf("Received: %s\n", data21);




    /*send(hClientSocket,"QUIT\r\n", strlen("QUIT\r\n"), 0);
    printf("Send QUIT\n");
    char* data11 = malloc(2048);
    recv(hClientSocket, data11, 2048, 0);
    printf("Received: %s\n", data11);*/

    printf("END\n");
    
    closesocket(hClientSocket);
    WSACleanup();
    return 0;
}

BOOL InitWinSock2_0()
{
    WSADATA wsaData;
    WORD wVersion = MAKEWORD(2, 0);
    if (!WSAStartup(wVersion, &wsaData))
        return TRUE;
    return FALSE;
}
