#include <stdio.h>
#include <strings.h>
#include <winsock2.h>
#include <stdint.h>
#include <stdlib.h>

void *doReceiving(void *sockID);
BOOL InitWinSock2_0();

// void send_file(SOCKET socket)
// {
//     FILE *fp;
//     char content[256];

//     // Opening file in reading mode
//     fp = fopen("text.txt", "r");

//     if (NULL == fp)
//     {
//         printf("file can't be opened \n");
//     }

//     printf("content of this file are \n");

//     while (fgets(content, sizeof(content), fp) != NULL)
//     {
//         printf("content: %s", content);
//         send(socket, content, strlen(content), 0);
//     }

//     // Closing the file
//     fclose(fp);
// }
int nServerPort = 21;

int main()
{
    char *szServerIPAddr = malloc(2048); // Dia chi ID cua server
    char *user = malloc(2048);
    char *pass = malloc(2048);
    char *fileName = malloc(2048);
    printf("Enter IP Address : ");
    scanf("\n");
    scanf("%[^\n]", szServerIPAddr);
    printf("Enter username: ");
    scanf("\n");
    scanf("%[^\n]", user);
    printf("Enter password: ");
    scanf("\n");
    scanf("%[^\n]", pass);
    printf("Enter file name  : ");
    scanf("\n");
    scanf("%[^\n]", fileName);


    

    //in ra szIP and 
    printf("\n%s\t%d\n", szServerIPAddr, nServerPort);

    if (!InitWinSock2_0())
    {
        printf("Unable to Initialize Windows Socket environment, ERRORCODE = %d\n",
               WSAGetLastError());
        return -11;
    }

    SOCKET hClientSocket;
    hClientSocket = socket(AF_INET, SOCK_STREAM, 0);
    printf("%d\n", hClientSocket);
    if (hClientSocket == INVALID_SOCKET)
    {
        printf("Unable to create Server socket\n");
        // Cleanup the environment initialized by WSAStartup()
        WSACleanup();
        return -12;
    }

    // Create the structure describing various Server parameters
    struct sockaddr_in serverAddr;

    serverAddr.sin_family = AF_INET; // The address family. MUST be AF_INET
    serverAddr.sin_addr.s_addr = inet_addr(szServerIPAddr);
    serverAddr.sin_port = htons(nServerPort);

    // Connect to the server
    if (connect(hClientSocket, (struct sockaddr *)&serverAddr,
                sizeof(serverAddr)) < 0)
    {
        printf("Unable to connect to %s on port %d\n", szServerIPAddr, nServerPort);
        closesocket(hClientSocket);
        WSACleanup();
        return -13;
    }

    printf("Connection established ............\n");

    char *szBuffer = malloc(2048);
    recv(hClientSocket, szBuffer, 2048, 0);
    printf("Received: %s\n", szBuffer);
    if (strstr(szBuffer, "220") == NULL)
    {
        return 220;
    }

    char data[2048];
    char send1[2048];
    sprintf(send1, "USER %s\r\n", user);
    send(hClientSocket, send1, strlen(send1), 0);
    printf("Send username: %s\n", user);
    char data1[2048];
    recv(hClientSocket, data1, 2048, 0);
    printf("Received: %s\n", data1);

    char send2[2048];
    sprintf(send2, "PASS %s\r\n", pass);
    send(hClientSocket, send2, strlen(send2), 0);
    printf("Send password: %s\n", pass);
    char data2[2048];
    recv(hClientSocket, data2, 2048, 0);
    printf("Received: %s\n", data2);

    send(hClientSocket, "CWD /\r\n", strlen("CWD /\r\n"), 0);
    printf("Send CWD /\n");
    char data7[2048];
    recv(hClientSocket, data7, 2048, 0);
    printf("Received: %s\n", data7);

    send(hClientSocket, "PWD\r\n", strlen("PWD\r\n"), 0);
    printf("Send PWD\n");
    char data4[2048];
    recv(hClientSocket, data4, 2048, 0);
    printf("Received: %s\n", data4);

    send(hClientSocket, "TYPE A\r\n", strlen("TYPE A\r\n"), 0);
    printf("Send TYPE\n");
    char data5[2048];
    recv(hClientSocket, data5, 2048, 0);
    printf("Received: %s\n", data5);

    send(hClientSocket, "PASV\r\n", strlen("PASV\r\n"), 0);
    printf("Send PASV\n");
    char data6[2048];
    recv(hClientSocket, data6, 2048, 0);
    printf("Received: %s\n", data6);

    int port = 0, port1 = 0, port2 = 0, dem = 0;
    for (int i = 0; i < strlen(data6); i++)
    {
        if (data6[i] == ',')
        {
            dem++;
            continue;
        }
        if (dem == 4)
        {
            port1 = port1 * 10 + data6[i] - 48;
        }
        if (data6[i] == (')'))
            break;
        if (dem == 5)
        {
            port2 = port2 * 10 + data6[i] - 48;
        }
    }
    port = port1 * 256 + port2;
    printf("Received port: %d\n", port);

    char send3[2048];
    sprintf(send3, "STOR %s\r\n", fileName);
    send(hClientSocket, send3, strlen(send3), 0);
    printf("Send %s\n", send3);
    char data8[2048];
    recv(hClientSocket, data8, 2048, 0);
    printf("Received: %s\n", data8);

    // Create New Socket
    SOCKET hClientSocket2;
    hClientSocket2 = socket(AF_INET, SOCK_STREAM, 0);
    printf("%d\n", hClientSocket2);
    if (hClientSocket2 == INVALID_SOCKET)
    {
        printf("Unable to create Server socket\n");
        // Cleanup the environment initialized by WSAStartup()
        WSACleanup();
        return -12;
    }

    // Create the structure describing various Server parameters
    struct sockaddr_in serverAddr2;

    serverAddr2.sin_family = AF_INET; // The address family. MUST be AF_INET
    serverAddr2.sin_addr.s_addr = inet_addr(szServerIPAddr);
    serverAddr2.sin_port = htons(port);

    // Connect to the server
    if (connect(hClientSocket2, (struct sockaddr *)&serverAddr2,
                sizeof(serverAddr2)) < 0)
    {
        printf("Unable to connect to %s on port %d\n", szServerIPAddr, port);
        closesocket(hClientSocket2);
        WSACleanup();
        return -13;
    }

    printf("Connection 2 established ............\n");
    //enter file to send
    FILE *File;
    File = fopen (fileName, "rb");
    if(!File) {
        printf ("Error while readaing the file\n");
    } else {
        // printf ("File opened successfully!\n");
        fseek(File, 0, SEEK_END);
        unsigned long Size = ftell(File);
        fseek(File, 0, SEEK_SET);
        char *Buffer = malloc(Size);

        fread(Buffer, Size, 1, File);
        fclose(File);
        send(hClientSocket2, Buffer, Size, 0); // File Binary
        printf("Sent: file\n\n");
        free(Buffer);
    }
    closesocket(hClientSocket2);
    printf("\n");

    char data9[2048];
    recv(hClientSocket, data9, 2048, 0);
    printf("Received: %s\n", data9);

    // QUIT
    send(hClientSocket, "QUIT\r\n", strlen("QUIT\r\n"), 0);
    printf("QUIT\n");
    char data10[2048];
    recv(hClientSocket, data10, 2048, 0);
    printf("Received: %s\n", data10);
    closesocket(hClientSocket);
    WSACleanup();
    return 0;
}

BOOL InitWinSock2_0()
{
    WSADATA wsaData;
    WORD wVersion = MAKEWORD(2, 0);
    if (!WSAStartup(wVersion, &wsaData))
        return TRUE;
    return FALSE;
}
